﻿using Lab2.Implementations;

namespace Lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            var app = new Application(new DialogShapeController());
            app.Run();
        }
    }
}
