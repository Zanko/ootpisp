﻿using Lab2.Contracts;
using Lab2.Extensions;
using System;

namespace Lab2
{
    class Application
    {
        private const int CreateShapeCommand = 1;
        private const int ShowShapesCommand = 2;
        private const int ExitCommand = 3;

        private readonly IShapeController _shapeController;

        public Application(IShapeController shapeController)
        {
            _shapeController = shapeController ?? throw new ArgumentNullException(nameof(shapeController));
        }

        /// <summary>
        /// Start running application
        /// </summary>
        public void Run()
        {
            while (true)
            {
                ShowMenu();
                var userCommand = ConsoleExtensions.WaitUserCommand(commandNumber => commandNumber >= 1 && commandNumber <= 3);
                if (userCommand == ExitCommand)
                    return;
                try
                {
                    RunCommand(userCommand);
                }
                catch(Exception ex)
                {
                    ConsoleExtensions.WriteError(ex.Message);
                    ConsoleExtensions.ShowRefreshMessage();
                }
            }            
        }        

        /// <summary>
        /// Show menu in console
        /// </summary>
        private void ShowMenu()
        {
            Console.Clear();
            Console.WriteLine("----------Menu----------");
            Console.WriteLine("1. Create shape");
            Console.WriteLine("2. Show shapes");
            Console.WriteLine("3. Exit");
            Console.WriteLine();
        }

        /// <summary>
        /// Wait user command
        /// </summary>
        /// <returns>Command number</returns>
        private int WaitUserCommand()
        {
            while (true)
            {
                Console.Write("Enter command number: ");
                var userCommand = Console.ReadLine();

                int commandNumber;
                if (int.TryParse(userCommand, out commandNumber))
                {
                    if (commandNumber < 1 || commandNumber > 3)
                        ConsoleExtensions.WriteError("Command not exist");
                    else
                        return commandNumber;
                }
                else
                    ConsoleExtensions.WriteError("Invalid command format");
            }            
        }        

        /// <summary>
        /// Run user command
        /// </summary>
        /// <param name="command">User command</param>
        private void RunCommand(int command)
        {
            switch (command)
            {
                case CreateShapeCommand:
                    _shapeController.CreateShape();
                    ConsoleExtensions.ShowRefreshMessage();
                    break;
                case ShowShapesCommand:                    
                    _shapeController.ShowShapes();
                    ConsoleExtensions.ShowRefreshMessage();
                    break;                
                default:
                    throw new ArgumentOutOfRangeException(nameof(command));
            }
        }
    }
}
