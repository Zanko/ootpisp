﻿using System;

namespace Lab2.Extensions
{
    static class ConsoleExtensions
    {
        /// <summary>
        /// Show error message
        /// </summary>
        /// <param name="message">Error message</param>
        public static void WriteError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.WriteLine();
            Console.ResetColor();
        }

        /// <summary>
        /// Wait user command
        /// </summary>
        /// <returns>Command number</returns>
        public static int WaitUserCommand(Func<int,bool> existCommand)
        {
            if (existCommand == null)
                throw new ArgumentNullException(nameof(existCommand));

            while (true)
            {
                Console.Write("Enter command number: ");
                var userCommand = Console.ReadLine();

                int commandNumber;
                if (int.TryParse(userCommand, out commandNumber))
                {
                    if(!existCommand(commandNumber))
                        WriteError("Command not exist");
                    else
                        return commandNumber;
                }
                else
                    WriteError("Invalid command format");
            }
        }

        /// <summary>
        /// Show refresh message
        /// </summary>
        public static void ShowRefreshMessage()
        {
            Console.WriteLine("Enter any key for refresh menu");
            Console.ReadKey();
        }
    }
}
