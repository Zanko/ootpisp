﻿using Creators;
using Lab2.Contracts;
using Lab2.Extensions;
using Shapes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Lab2.Implementations
{
    class DialogShapeController : IShapeController
    {
        private List<Shape> _shapes;
        private List<IShapeCreator> _shapesCreators = new List<IShapeCreator>();
        private Type _shapeCreatorInterfaceType = typeof(IShapeCreator);
        private int _exitCommand = 1;

        public DialogShapeController()
        {
            _shapes = new List<Shape>();
        }

        /// <summary>
        /// Create shape
        /// </summary>
        public void CreateShape()
        {
            LoadShapeTypes();
            CreateShapeMenuShow();
            int userCommand = ConsoleExtensions.WaitUserCommand(commandNumber => commandNumber >= 1 && commandNumber <= _shapesCreators.Count + 1);
            if (userCommand == _exitCommand)
                return;
            try
            {
                CreateShape(userCommand - 1);
            }
            catch (Exception ex)
            {
                ConsoleExtensions.WriteError(ex.Message);                
            }
        }

        /// <summary>
        /// Show shapes
        /// </summary>
        public void ShowShapes()
        {
            Console.Clear();
            foreach (var shape in _shapes)
                Console.WriteLine(shape.GetType().Name);
        }

        private void CreateShape(int shapeCreatorIndex)
        {
            Console.Clear();
            _shapes.Add(_shapesCreators[shapeCreatorIndex].Create());
        }       

        /// <summary>
        /// Show title for create shape
        /// </summary>
        private void CreateShapeMenuShow()
        {
            Console.Clear();
            Console.WriteLine("----------Create shape----------");
            for(var i=0; i  < _shapesCreators.Count; i++)
            {
                Console.WriteLine($"{i + 1}. Create {_shapesCreators[i].ShapeName.ToLower()}");
            }
            Console.WriteLine($"{_exitCommand}. Exit");
            Console.WriteLine();
        }

        /// <summary>
        /// Load shapes type
        /// </summary>
        private void LoadShapeTypes()
        {
            if(_shapesCreators.Count < 1)
            {
                foreach (var type in Assembly.GetAssembly(_shapeCreatorInterfaceType).GetTypes()
                    .Where(t => _shapeCreatorInterfaceType.IsAssignableFrom(t) && !t.IsInterface && !t.IsAbstract))
                {
                    _shapesCreators.Add((IShapeCreator)Activator.CreateInstance(type));
                }                
                _exitCommand = _shapesCreators.Count + 1;
            }
        }        
    }
}
