﻿namespace Lab2.Contracts
{
    interface IShapeController
    {
        /// <summary>
        /// Create shape
        /// </summary>
        void CreateShape();

        /// <summary>
        /// Show shapes
        /// </summary>
        void ShowShapes();
    }
}
