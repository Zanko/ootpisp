﻿using System;
using System.Drawing;

namespace Shapes
{
    public class Triangle : Shape
    {
        private Point _pointA;
        private Point _pointB;
        private Point _pointC;

        public Triangle(Point pointA, Point pointB, Point pointC)
        {
            _pointA = pointA;
            _pointB = pointB;
            _pointC = pointC;            
        }

        public void SetPointA(Point pointA)
        {
            _pointA = pointA;
        }

        public Point GetPointA()
        {
            return _pointA;
        }

        public void SetPointB(Point pointB)
        {
            _pointB = pointB;
        }

        public Point GetPointB()
        {
            return _pointB;
        }

        public void SetPointC(Point pointC)
        {
            _pointC = pointC;
        }

        public Point GetPointC()
        {
            return _pointC;
        }

        public override void Draw()
        {
            Console.WriteLine($"Triangle({_pointA}, {_pointB}, {_pointC})");
        }
    }
}
