﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Shapes
{
    public class Polygon : Shape
    {
        private IEnumerable<Point> _pointsList;       

        public Polygon(IEnumerable<Point> pointsList)
        {
            SetPolygonPoints(pointsList);
        }

        public void SetPolygonPoints(IEnumerable<Point> pointsList)
        {
            _pointsList = pointsList ?? throw new NullReferenceException($"Argument '{nameof(pointsList)}' can not be null");
        }

        public IEnumerable<Point> GetPolygonPoints(IEnumerable<Point> pointsList)
        {
            return _pointsList.Select(p=>p);
        }

        public override void Draw()
        {
            Console.WriteLine($"Polygon({string.Join(", ", _pointsList)})");
        }
    }
}
