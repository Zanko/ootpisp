﻿using System;
using System.Drawing;

namespace Shapes
{
    public class Rectangle : Shape
    {
        private Point _pointA;
        private Point _pointB;
        private Point _pointC;
        private Point _pointD;

        public Rectangle(Point pointA, Point pointC)
        {
            _pointA = pointA;            
            _pointC = pointC;
            CalculatePointsBD();
        }

        public void SetPointA(Point pointA)
        {
            _pointA = pointA;
            CalculatePointsBD();
        }

        public Point GetPointA()
        {
            return _pointA;
        }

        public void SetPointC(Point pointC)
        {
            _pointC = pointC;
            CalculatePointsBD();
        }

        public Point GetPointC()
        {
            return _pointC;
        }

        private void CalculatePointsBD()
        {
            _pointB = new Point(_pointC.X, _pointA.Y);
            _pointD = new Point(_pointA.X, _pointC.Y);
        }

        public override void Draw()
        {
            Console.WriteLine($"Rectangle({_pointA}, {_pointB}, {_pointC}, {_pointD})");
        }
    }
}
