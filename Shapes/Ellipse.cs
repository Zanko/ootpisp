﻿using System;
using System.Drawing;

namespace Shapes
{
    public class Ellipse : Shape
    {
        private Point _pointA;
        private Point _pointB;
        private Point _pointC;
        private Point _pointD;

        public Ellipse(Point pointA, Point pointB)
        {
            _pointA = pointA;
            _pointB = pointB;
            CalculatePointsCD();
        }

        public void SetPointA(Point pointA)
        {
            _pointA = pointA;
            CalculatePointsCD();
        }

        public Point GetPointA()
        {
            return _pointA;
        }

        public void SetPointB(Point pointB)
        {
            _pointB = pointB;
            CalculatePointsCD();
        }

        public Point GetPointB()
        {
            return _pointB;
        }

        private void CalculatePointsCD()
        {
            _pointC = new Point(_pointB.X + (_pointB.X - _pointA.X), _pointA.Y);
            _pointD = new Point(_pointB.X, (_pointA.Y - (_pointB.Y - _pointA.Y)));
        }

        public override void Draw()
        {
            Console.WriteLine($"Ellipse({_pointA}, {_pointB}, {_pointC}, {_pointD})");
        }
    }
}
