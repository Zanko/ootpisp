﻿using System.Collections.Generic;
using System.Drawing;

namespace Shapes
{
    public static class ShapesCollection
    {
        private static List<Shape> _shapes = new List<Shape>
            {
                new Triangle(new Point(0,0), new Point(1,1), new Point(2,0)),
                new Rectangle(new Point(0,1), new Point(1,0)),
                new Polygon(new List<Point>{new Point(0,0), new Point(1,1), new Point(2,1)}),
                new Line(new Point(0,0), new Point(1,1)),
                new Ellipse(new Point(0,0), new Point(1,1)),
                new Circle(new Point(0,0), 2)
            };        

        public static void Draw()
        {
            foreach (var shape in _shapes)
                shape.Draw();
        }
    }
}
