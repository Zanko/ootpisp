﻿using System;
using System.Drawing;

namespace Shapes
{
    public class Line : Shape
    {
        private Point _pointA;
        private Point _pointB;

        public Line(Point pointA, Point pointB)
        {
            _pointA = pointA;
            _pointB = pointB;
        }

        public void SetPointA(Point pointA)
        {
            _pointA = pointA;
        }

        public Point GetPointA()
        {
            return _pointA;
        }

        public void SetPointB(Point pointB)
        {
            _pointB = pointB;
        }

        public Point GetPointB()
        {
            return _pointB;
        }

        public override void Draw()
        {
            Console.WriteLine($"Line({_pointA}, {_pointB})");
        }
    }
}
