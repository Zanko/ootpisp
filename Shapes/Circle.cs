﻿using System;
using System.Drawing;
using System.Linq;

namespace Shapes
{
    public class Circle : Shape
    {
        private Point _centerPoint;
        private double _radius;
        private readonly int[] _angles = new[] { 0, 60, 120, 180, 240, 300 };
        private const string RadiusErrorMessage = "Radius should be more than 0";

        public Circle(Point centerPoint, double radius)
        {
            SetCenterPoint(centerPoint);
            SetRadius(radius);                   
        }

        public void SetCenterPoint(Point centerPoint)
        {
            _centerPoint = centerPoint;
        }

        public Point GetCenterPoint()
        {
            return _centerPoint;
        }

        public void SetRadius(double radius)
        {
            if (radius < 0)
                throw new ArgumentException(RadiusErrorMessage);
            _radius = radius;
        }

        public double GetRadius()
        {
            return _radius;
        }

        public override void Draw()
        {
            Console.WriteLine($"Circle({string.Join(", ", _angles.Select(a=> new Point(CalculateX(a),CalculateY(a))))})");
        }

        private double DegreeToRadian(double angle)
        {
            return Math.PI * angle / 180.0;
        }

        private int CalculateX(int angle)
        {
            return (int)(_centerPoint.X + (_radius * Math.Cos(DegreeToRadian(angle))));
        }

        private int CalculateY(int angle)
        {
            return (int)(_centerPoint.Y + (_radius * Math.Sin(DegreeToRadian(angle))));
        }
    }
}
