﻿using Creators.Extensions;
using Shapes;
using System.Collections.Generic;
using System.Drawing;

namespace Creators
{
    class PolygonCreator : IShapeCreator
    {
        public string ShapeName => "Polygon";

        //// <summary>
        /// Create polygon
        /// </summary>
        /// <returns>Polygon</returns>
        public Shape Create()
        {
            var pointCount = ConsoleExtensions.GetInt32FromUser("points count");
            var pointList = new List<Point>();
            for (int i = 0; i < pointCount; i++)
            {
                pointList.Add(new Point(ConsoleExtensions.GetInt32FromUser($"P{i + 1}.x"), ConsoleExtensions.GetInt32FromUser($"P{i + 1}.y")));
            }            
            return new Polygon(pointList);
        }
    }
}
