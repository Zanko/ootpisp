﻿using Creators.Extensions;
using Shapes;
using System.Drawing;

namespace Creators
{
    class LineCreator : IShapeCreator
    {
        public string ShapeName => "Line";

        //// <summary>
        /// Create line
        /// </summary>
        /// <returns>Line</returns>
        public Shape Create()
        {
            var pointA = new Point(ConsoleExtensions.GetInt32FromUser("A.x"), ConsoleExtensions.GetInt32FromUser("A.y"));
            var pointB = new Point(ConsoleExtensions.GetInt32FromUser("B.x"), ConsoleExtensions.GetInt32FromUser("B.y"));
            return new Line(pointA, pointB);
        }
    }
}
