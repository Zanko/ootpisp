﻿using Shapes;

namespace Creators
{
    public interface IShapeCreator
    {
        /// <summary>
        /// Create shape
        /// </summary>
        /// <returns>Shape</returns>
        Shape Create();

        /// <summary>
        /// Shape name that could is created
        /// </summary>
        string ShapeName { get; }
    }
}
