﻿using Creators.Extensions;
using Shapes;
using System.Drawing;

namespace Creators
{
    class TriangleCreator : IShapeCreator
    {
        public string ShapeName => "Triangle";

        //// <summary>
        /// Create triangle
        /// </summary>
        /// <returns>Triangle</returns>
        public Shape Create()
        {
            var pointA = new Point(ConsoleExtensions.GetInt32FromUser("A.x"), ConsoleExtensions.GetInt32FromUser("A.y"));
            var pointB = new Point(ConsoleExtensions.GetInt32FromUser("B.x"), ConsoleExtensions.GetInt32FromUser("B.y"));
            var pointC = new Point(ConsoleExtensions.GetInt32FromUser("C.x"), ConsoleExtensions.GetInt32FromUser("C.y"));
            return new Triangle(pointA, pointB, pointC);
        }
    }
}
