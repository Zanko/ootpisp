﻿using System.Drawing;
using Creators.Extensions;
using Shapes;

namespace Creators
{
    class CircleCreator : IShapeCreator
    {
        public string ShapeName => "Circle";

        //// <summary>
        /// Create circle
        /// </summary>
        /// <returns>Circle</returns>
        public Shape Create()
        {
            var point = new Point(ConsoleExtensions.GetInt32FromUser("O.x"), ConsoleExtensions.GetInt32FromUser("O.y"));
            var radius = ConsoleExtensions.GetDoubleFromUser("R");
            return new Circle(point, radius);
        }        
    }
}
