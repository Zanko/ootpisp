﻿using System;

namespace Creators.Extensions
{
    static class ConsoleExtensions
    {
        /// <summary>
        /// Wait user Int32 value
        /// </summary>
        /// <param name="valueName">Value name</param>
        /// <returns>Value was enterred by user</returns>
        public static int GetInt32FromUser(string valueName)
        {
            Console.Write($"Enter {valueName}: ");
            int x;
            if (!int.TryParse(Console.ReadLine(), out x))
                throw new FormatException("Invalid number format");
            return x;
        }

        /// <summary>
        /// Wait user Double value
        /// </summary>
        /// <param name="valueName">Value name</param>
        /// <returns>Value was enterred by user</returns>
        public static double GetDoubleFromUser(string valueName)
        {
            Console.Write($"Enter {valueName}: ");
            double x;
            if (!double.TryParse(Console.ReadLine(), out x))
                throw new FormatException("Invalid number format");
            return x;
        }
    }
}
