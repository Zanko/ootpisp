﻿using Creators.Extensions;
using Shapes;
using System.Drawing;
using Rectangle = Shapes.Rectangle;

namespace Creators
{
    class RectangleCreator : IShapeCreator
    {
        public string ShapeName => "Rectangle";

        //// <summary>
        /// Create rectangle
        /// </summary>
        /// <returns>Rectangle</returns>
        public Shape Create()
        {
            var pointA = new Point(ConsoleExtensions.GetInt32FromUser("A.x"), ConsoleExtensions.GetInt32FromUser("A.y"));
            var pointC = new Point(ConsoleExtensions.GetInt32FromUser("C.x"), ConsoleExtensions.GetInt32FromUser("C.y"));
            return new Rectangle(pointA, pointC);
        }
    }
}
