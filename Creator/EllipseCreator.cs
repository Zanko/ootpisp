﻿using Creators.Extensions;
using Shapes;
using System.Drawing;

namespace Creators
{
    class EllipseCreator : IShapeCreator
    {
        public string ShapeName => "Ellipse";

        //// <summary>
        /// Create ellipse
        /// </summary>
        /// <returns>Ellipse</returns>
        public Shape Create()
        {
            var pointA = new Point(ConsoleExtensions.GetInt32FromUser("A.x"), ConsoleExtensions.GetInt32FromUser("A.y"));
            var pointB = new Point(ConsoleExtensions.GetInt32FromUser("B.x"), ConsoleExtensions.GetInt32FromUser("B.y"));
            return new Ellipse(pointA, pointB);
        }
    }
}
